const mongoose = require('mongoose')

const peopleKilledSchema = mongoose.Schema({
    name: String,
    age: String,
    race: String,
}, {
    collection: 'police_killings'
})

const PeopleKilled = mongoose.model('peopleKilled', peopleKilledSchema)


module.exports = PeopleKilled