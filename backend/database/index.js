const mongoose = require('mongoose')
const URI = 'mongodb://localhost:27017/data_police'
mongoose
  .connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log('Connexion to DB succeed'))
  .catch(err => console.log('Error: ', err))



//const URI = 'mongodb://localhost:27017/localApp'
