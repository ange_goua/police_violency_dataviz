const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParser = require('body-parser')
const routing = require('./routes')

require('./database')

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization'
  )
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PUT, DELETE, PATCH, OPTIONS'
  )
  next()
})

// middleware dev tool
app.use(morgan('dev'))
// middleware parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// middleware parse application/json
app.use(bodyParser.json())

app.use((err, req, res, next) => {
  res.status(500).send('Error: ', err)
})
app.use('/', routing)

app.listen(3000)
