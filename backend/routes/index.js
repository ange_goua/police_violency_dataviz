const router = require('express').Router()
const peopleKilledRouter = require('./peopleKilled.routes')

router.use('/api/peopleKilled', peopleKilledRouter)

module.exports = router