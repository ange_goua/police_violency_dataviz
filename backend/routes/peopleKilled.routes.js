const router = require('express').Router()
const PeopleKilled = require('../database/models/peopleKilled.model')


router.get('/', (req, res, next) => {
  try {
    PeopleKilled.find({},
      // {
      //   __v: 0
      // }
    ).then(data => {
        res.status(200).json(data)
        //console.log(data);
    })
  } catch (e) {
    next(e)
  }
})

module.exports = router


// router.post('/', (req, res, next) => {
//   const body = req.body
//   try {
//     if (!body.title || !body.message === 0) {
//       res.status(400).json(['Title and Message are required !'])
//     }

//     const newPost = new Post({
//         nom: body.nom,
//         age: body.age,
//         cause: body.cause,
//     })

//     newPost
//       .save()
//       .then(() => {
//         res.status(201).json({
//           message: 'Post was created'
//         })
//       })
//       .catch(err =>
//         res.status(400).json({
//           error: err
//         })
//       )
//   } catch (e) {
//     next(e)
//   }
// })