const path = require('path') //objet pour gerer les liens
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    devtool: 'source-map',
    devServer: {
        contentBase: './dist',
        open: true,
        //host : '0.0.0.0',
        //useLocalIp: true
    },
    entry: {
        'index': path.resolve(__dirname, '../src/index.js'),
        'story': path.resolve(__dirname, '../src/story.js'),
        'statistics': path.resolve(__dirname, '../src/statistics.js'),
    },
    output :
    {
        path : path.resolve(__dirname, '../dist'),
        filename: 'scripts/[name].[hash].js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(__dirname, '../src/index.html'),
            inject: true,
            chunks: ['index']
          }),

        new HtmlWebpackPlugin({
            filename: 'story.html',
            template: path.resolve(__dirname, '../src/story.html'),
            inject: true,
            chunks: ['story']
        }),

        new HtmlWebpackPlugin({
            filename: 'statistics.html',
            template: path.resolve(__dirname, '../src/statistics.html'),
            inject: true,
            chunks: ['statistics']
        }),

        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: "styles/[name].css",
            chunkFilename: "[id].css"
          }),
        new CopyWebpackPlugin([ { from: 'static'} ])
    ],
    module: {
        rules: [{
                test: /\.html$/,
                use: ['html-loader'],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.styl$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'stylus-loader'],
            },
            {
                test: /\.json5$/i,
                loader: 'json5-loader',
                type: 'javascript/auto',
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: 'images/'
                    }
                }]
            },
            {
                test: /\.glsl$/,
                use: [
                    'raw-loader',
                    'glslify-loader'
                ]
            },
            {
                test: /\.(ttf|woff|woff2|eot)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: 'fonts/'
                    }
                }]
            }
        ]
    }
}