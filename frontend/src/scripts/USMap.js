import axios from 'axios'
import "regenerator-runtime/runtime.js"
import usMap from '../json/usMap.json'
import usState from '../json/usStates.json'
import * as d3 from "d3"
import * as topojson from "topojson-client";
import { text } from 'd3'


let clickOut = false, dataState, image = new Image()
const containerStats = document.querySelectorAll('.container-stats')

export default class DataConverterUs{
    constructor(year) {
        this.uri = 'http://localhost:3000/'


        this.usMap

        this.year = year
        this.usDate = []

        this.gender = []
        this.ages = []
        this.male = []
        this.races = []
        this.name = []
        this.cause = []
        this.cities = []
        this.images = []

        this.victimInformations = []

        this.whitePeople = 0
        this.hispanicPeople = 0
        this.othersPeople = 0
        this.blackPeople = 0

        this.gunshotCause = 0
        this.taserCause = 0
        this.vehicleCause = 0
        this.beatenCause = 0
        this.otherCause = 0

        this.totalData = 0

        this.loaded = false

    }

    async getDataState(year) {
        try {

            const response = await axios.get(`${this.uri}api/peopleKilled`)
            const data = response.data

            this.regex = new RegExp(`^[0-9]{2}[\/][0-9]{2}[\/]${year}$`, "g");


            data.forEach(_data => {
                if(this.regex.test(_data.date))
                {
                    this.usDate.push(_data)
                }
            })


            this.usDate.forEach(_data => {
                this.genders = this.gender.push(_data.gender)
                    if (_data.gender === "Male") {
                    this.males = this.male.push(_data.gender)
                }
                if(_data.image != null && _data.image != undefined){
                    this.victimInformations.push( {
                        name :  _data.name,
                        age :  _data.age,
                        date :  _data.date,
                        city :  _data.city,
                        race :  _data.race,
                        description :  _data.description,
                        image :  _data.image,
                        cause :  _data.cause,
                    })

                }
                this.name.push(_data.name)

                this.races.push(_data.race)
                this.cause.push(_data.cause)
                this.cities.push(_data.city)
                this.ages.push(parseInt(_data.age))
            })

            this.ages.forEach(() => {
                const index = this.ages.findIndex(Number.isNaN)
                this.ages.splice(index, 1)
            })


            this.races.forEach( race => {
                if( race === "Black") this.blackPeople++
                else if( race === "Hispanic") this.hispanicPeople++
                else if( race === "White") this.whitePeople++
                else this.othersPeople++
            })

            this.cause.forEach( cause => {
                if( cause === "Gunshot") this.gunshotCause++
                else if( cause === "Taser") this.taserCause++
                else this.otherCause++
            })


            this.totalData = this.name.length

            const genders = this.gender.length
            const males = this.male.length


            const getPercentage = (partialValue, totalValue) => (partialValue * 100) / totalValue

            this.malePercentage = (Math.round(getPercentage(males, genders)))
            this.femalePercentage = (100 - this.malePercentage)

            const reducer = (accumulator, currentValue) => ((accumulator + currentValue))

            let averageAgeData = 0

            if (typeof this.ages !== 'undefined' && this.ages.length > 0){
                const totalAge = this.ages.reduce(reducer);
                averageAgeData = Math.round(totalAge / this.ages.length)
                // console.log("age average =" , averageAge)
            }

            this.loaded = true


            this.usStats(year, averageAgeData, this.totalData )
            this.usAllData()


        } catch (e) {
            console.log(e);
        }
    }
    usStats(year, averageAgeData, totalData ){
        const victimsTotal = d3.select(".victims-total")
        victimsTotal.text(totalData)
        victimsTotal.append('span').attr("class", "red")

        const averageAge = d3.select(".average-age")
        averageAge.text(averageAgeData)
        averageAge.append('span').attr("class", "red")

        this.usMap = new USMap(year , averageAgeData, totalData)
    }

    usAllData(){

        this.raceDatas = new RaceDatas(this.blackPeople,this.whitePeople, this.hispanicPeople, this.othersPeople)
        this.citiesDatas = new CitiesDatas(this.cities)
        this.deathReason = new DeathReason(this.gunshotCause,this.taserCause,this.taserCause)
        this.victimDatas = new VictimDatas(this.victimInformations)

        this.genderDeath = new GenderDeath(this.malePercentage,this.femalePercentage)
        console.log(this.genderDeath);

    }

    firstDelete(){
        this.usMap.delet()
    }
}

class dataConverterState{
    constructor(stateCode , year) {
        this.uri = 'http://localhost:3000/'

        this.gender = []
        this.ages = []
        this.male = []
        this.races = []
        this.name = []
        this.cause = []
        this.cities = []

        this.victimInformations = []
        this.stateData = []

        this.whitePeople = 0
        this.hispanicPeople = 0
        this.othersPeople = 0
        this.blackPeople = 0

        this.gunshotCause = 0
        this.taserCause = 0
        this.vehicleCause = 0
        this.beatenCause = 0
        this.otherCause = 0

        this.totalData = 0

        this.year = year
        this.stateCode = stateCode
        this.getDataState()

    }

    async getDataState() {

        try {

            const response = await axios.get(`${this.uri}api/peopleKilled`)
            const data = response.data

            this.regex = new RegExp(`^[0-9]{2}[\/][0-9]{2}[\/]${this.year}$`, "g");
            console.log(this.regex);

            data.forEach(_data => {
                if(this.regex.test(_data.date))
                {
                    if (_data.state === `${this.stateCode}`) {
                        this.stateData.push(_data)
                    }
                }
            })


            this.stateData.forEach(_data => {
                this.genders = this.gender.push(_data.gender)
                    if (_data.gender === "Male") {
                    this.males = this.male.push(_data.gender)
                }
                if(_data.image != null && _data.image != undefined){
                    this.victimInformations.push( {
                        name :  _data.name,
                        age :  _data.age,
                        date :  _data.date,
                        city :  _data.city,
                        race :  _data.race,
                        description :  _data.description,
                        image :  _data.image,
                        cause :  _data.cause,
                    })

                }
                this.name.push(_data.name)

                this.races.push(_data.race)
                this.cause.push(_data.cause)
                this.cities.push(_data.city)
                this.ages.push(parseInt(_data.age))
            })

            this.ages.forEach(() => {
                const index = this.ages.findIndex(Number.isNaN)
                this.ages.splice(index, 1)
            })

            this.races.forEach( race => {
                if( race === "Black") this.blackPeople++
                else if( race === "Hispanic") this.hispanicPeople++
                else if( race === "White") this.whitePeople++
                else this.othersPeople++
            })

            this.cause.forEach( cause => {
                if( cause === "Gunshot") this.gunshotCause++
                else if( cause === "Taser") this.taserCause++
                else this.otherCause++
            })

            this.totalData = this.name.length

            const genders = this.gender.length
            const males = this.male.length


            const getPercentage = (partialValue, totalValue) => (partialValue * 100) / totalValue

            this.malePercentage = (Math.round(getPercentage(males, genders)))
            this.femalePercentage = (100 - this.malePercentage)

            const reducer = (accumulator, currentValue) => ((accumulator + currentValue))

            let averageAgeData = 0
            if (typeof this.ages !== 'undefined' && this.ages.length > 0){
                const totalAge = this.ages.reduce(reducer);
                averageAgeData = Math.round(totalAge / this.ages.length)
            }

            this.putData(averageAgeData, this.totalData)

        } catch (e) {
            console.log(e);
        }
    }
    putData(averageAgeData, totalData ){

        if(averageAgeData === undefined) averageAgeData ='No '
        if(totalData === undefined) totalData ='No '

        const victimsTotal = d3.select(".victims-total")
        victimsTotal.text(totalData)
        victimsTotal.append('span').attr("class", "red")

        const averageAge = d3.select(".average-age")
        averageAge.text(averageAgeData)
        averageAge.append('span').attr("class", "red")

    }

    stateAllData(){

        const raceStateDatas = new RaceDatas(this.blackPeople,this.whitePeople, this.hispanicPeople, this.othersPeople, '-state')
        const citiesStateDatas = new CitiesDatas(this.cities, '-state')
        const deathStateReason = new DeathReason(this.gunshotCause,this.taserCause,this.taserCause, '-state')

        const victimDatas = new VictimDatas(this.victimInformations, '-state')
        const genderDeath = new GenderDeath(this.malePercentage,this.femalePercentage, '-state')


    }
}




class USMap {
    constructor(year, averageAgeData, totalData){

        this.width = 960 * 0.75
        this.height = 500 * 0.75

        this.stateName
        this.stateCode

        this.year = year

        this.prevVictimsTotal = totalData
        this.prevAverageAge = averageAgeData

        this.victimsTotal = document.querySelector('.victims-total')
        this.averageAge = document.querySelector('.average-age')

        this.containerColumnCenter = document.querySelector('.container-column-center')

        this.drawMap(this.year)
    }

    drawMap(year)
    {
        const width = this.width, height = this.height

        let centered

        const projection = d3.geoAlbersUsa();

        const path = d3.geoPath().projection(projection);

        const statesText = d3.select(".states-text")
        .classed('states-text', true)


        const svg = d3.select(".container-svg").append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("class", "svgMap")
        .style("fill", "#222222")


        const color = d3.scaleThreshold()
        .domain([0.28, 0.38, 0.48, 0.58, 0.68, 0.78])
        .range(['#4d9221', '#a1d76a', '#e6f5d0', '#f7f7f7', '#fde0ef', '#e9a3c9', '#c51b7d'])

        const states = topojson.feature(usMap, usMap.objects.states);

        projection.fitSize([width, height], states);

        svg.selectAll("path")
            .data(states.features)
            .enter()
            .append("path")
            .attr("d", path)
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("click", clicked)

        svg.append("path")
            .datum(topojson.mesh(usMap, usMap.objects.states, (a, b) => a !== b))
            .attr("class", "mesh")
            .attr("d", path)

        const nameState = (d) => {

            usState.features.forEach(state => {
                if(state.id === d){
                    this.stateName = state.properties.name
                    this.stateCode = state.properties.code
                    return
                }
            })
            this.getStateCode(this.stateCode, year)
            return this.stateName
        }

        function mouseover (d){
            d3.select(this).style('fill', d => d === centered ? '#FF005C' : '#ff9900');
        }

        function mouseout (d){
            d3.select(this).style("fill", d => d === centered ? '#FF005C' : "#222222")
        }

        const _this = this

        function clicked (d) {

            centered = centered !== d && d;
            if(clickOut === false)
            {
                statesText.text(nameState(d.id))
                setTimeout(() => statesText.style("opacity", 1.0) ,900)
                changeLink()
                clickOut = true

            }else if(clickOut === true){
                statesText.style("opacity", 0)
                clickOut = false
                changeLink()

                _this.victimsTotal.innerHTML = `${_this.prevVictimsTotal} <span class="red"></span>`
                _this.averageAge.innerHTML = `${_this.prevAverageAge} <span class="red"></span>`
            }

            d3.select(this).style('fill', d => d === centered ? '#FF005C' : '#222222');


            const paths = svg.selectAll("path")
            .classed("unactive", d => d === centered)
            .classed("active", centered)

            // Starting translate/scale
            const t0 = projection.translate(),
            s0 = projection.scale();

            // Re-fit to destination
            projection.fitSize([width, height], centered || states);

            // Create interpolators
            const interpolateTranslate = d3.interpolate(t0, projection.translate()),
                interpolateScale = d3.interpolate(s0, projection.scale());

            const interpolator = (t) => {
            projection.scale(interpolateScale(t))
                .translate(interpolateTranslate(t));
            paths.attr("d", path);
            }

            d3.transition()
                .duration(750)
                .tween("projection", () => {
                    return interpolator;
                })
        }


    }
    getStateCode(stateCode, year){
        dataState = new dataConverterState(stateCode, year)
    }

    delet(){
        d3.select(".container-svg").selectAll("*").remove();
    }
}


class RaceDatas{
    constructor( blackPeople, whitePeople, hispanicPeople, othersPeople, when){

        this.blackPeople = blackPeople
        this.whitePeople = whitePeople
        this.hispanicPeople = hispanicPeople
        this.othersPeople = othersPeople
        this.when = when || ''


        const allDeath = blackPeople + whitePeople + hispanicPeople + othersPeople

        whitePeople = parseInt((whitePeople*100)/allDeath)
        blackPeople = parseInt((blackPeople*100)/allDeath)
        hispanicPeople = parseInt((hispanicPeople*100)/allDeath)
        othersPeople = parseInt((othersPeople*100)/allDeath)


        const dataWhite = []
        const dataBlack = []
        const dataHispanic = []
        const dataOther = []

        dataWhite.push(whitePeople)
        dataBlack.push(blackPeople)
        dataHispanic.push(hispanicPeople)
        dataOther.push(othersPeople)


            dataWhite.forEach(_element => {
                let animationDuration = 400,
                barWidth = whitePeople*4

                let svg

                if (this.when === '-state') svg = d3.select(".js-chart-white-state")
                else svg = d3.select(".js-chart-white")

                svg.selectAll("*").remove();


                let bar = svg.append("rect")
                    .attr("class", "bar-white")
                    .attr("height", () => {
                        return 0
                    })
                    .attr("x", 29 + '%')
                    .attr("width", 100)
                    .attr("alignment-baseline", 'middle')

                svg.append("text")
                    .text(whitePeople + '%')
                    .attr("class", "text-chart")
                    .attr("fill", "white")
                    .attr("font-weight", "bold")
                    .attr("font-size", 26 + "px")
                    .attr("font-family", "Open Sans Condensed")
                    .attr("x", -63 + '%')
                    .attr("y", -10 + '%')

                containerStats.forEach(container => container.addEventListener("mouseover", toggleBar))

                function toggleBar() {
                    let transition = bar.transition().duration(animationDuration)
                        transition.attr("y", 0).attr("height", barWidth)
                    }
        })

        dataBlack.forEach(_element => {
            let animationDuration = 400,
            barWidth = blackPeople*4

            let svg
            if (this.when === '-state') svg = d3.select(".js-chart-black-state")
            else svg = d3.select(".js-chart-black")

            svg.selectAll("*").remove();


            let bar = svg.append("rect")
            .attr("class", "bar-black")
            .attr("height", () => {
                return 0
            })
            .attr("x", 29 + '%')
            .attr("width", 100)

            svg.append("text")
                .text(blackPeople + '%')
                .attr("class", "text-chart")
                .attr("fill", "white")
                .attr("font-weight", "bold")
                .attr("font-size", 26 + "px")
                .attr("font-family", "Open Sans Condensed")
                .attr("x", -63 + '%')
                .attr("y", -10 + '%')



            containerStats.forEach(container => container.addEventListener("mouseover", toggleBar))


            function toggleBar() {
                let transition = bar.transition().duration(animationDuration)
                    transition.attr("y", 0).attr("height", barWidth)
                }

            })

            dataHispanic.forEach(_element => {
                let animationDuration = 400,
                barWidth = hispanicPeople*4

                let svg
                if (this.when === '-state') svg = d3.select(".js-chart-hispanic-state")
                else svg = d3.select(".js-chart-hispanic")

                svg.selectAll("*").remove();


                let bar = svg.append("rect")
                .attr("class", "bar-hispanic")
                .attr("height", () => {
                    return 0
                })
                .attr("width", 100)
                .attr("x", 29 + '%')

                svg.append("text")
                    .text(hispanicPeople + '%')
                    .attr("class", "text-chart")
                    .attr("fill", "white")
                    .attr("font-weight", "bold")
                    .attr("font-size", 26 + "px")
                    .attr("font-family", "Open Sans Condensed")
                    .attr("x", -63 + '%')
                    .attr("y", -10 + '%')

                containerStats.forEach(container => container.addEventListener("mouseover", toggleBar))


                function toggleBar() {
                    let transition = bar.transition().duration(animationDuration)
                        transition.attr("y", 0).attr("height", barWidth)
                    }
                })

                dataOther.forEach(_element => {
                    let animationDuration = 400,
                    barWidth = othersPeople*4


                    let svg
                    if (this.when === '-state') svg = d3.select(".js-chart-other-state")
                    else svg = d3.select(".js-chart-other")

                    svg.selectAll("*").remove();


                    let bar = svg.append("rect")
                    .attr("class", "bar-other")
                    .attr("height", () => {
                        return 0
                    })
                    .attr("x", 29 + '%')
                    .attr("width", 100)

                    svg.append("text")
                        .text(othersPeople + '%')
                        .attr("class", "text-chart")
                        .attr("fill", "white")
                        .attr("font-weight", "bold")
                        .attr("font-size", 26 + "px")
                        .attr("font-family", "Open Sans Condensed")
                        .attr("x", -63 + '%')
                        .attr("y", -10 + '%')

                    containerStats.forEach(container => container.addEventListener("mouseover", toggleBar))

                    function toggleBar() {
                        let transition = bar.transition().duration(animationDuration)
                            transition.attr("y", 0).attr("height", barWidth)
                        }
        })
    }

}

class DeathReason{
    constructor(gunshotCause, taserCause, otherCause, when){

        this.gunshotCause = gunshotCause
        this.taserCause = taserCause
        this.otherCauser = otherCause
        this.when = when || ''

        let totalDeathCause = 0

        totalDeathCause = gunshotCause + taserCause + otherCause
        gunshotCause = parseInt((gunshotCause*100)/totalDeathCause)
        taserCause = parseInt((taserCause*100)/totalDeathCause)
        otherCause = parseInt((otherCause*100)/totalDeathCause)

        let gunshotDeath, taserDeath, otherDeath

        if (this.when === '-state') {
            gunshotDeath = document.querySelector('.text-gunshot-state')
            taserDeath = document.querySelector('.text-taser-state')
            otherDeath = document.querySelector('.text-other-death-state')
        }else{
            gunshotDeath = document.querySelector('.text-gunshot')
            taserDeath = document.querySelector('.text-taser')
            otherDeath = document.querySelector('.text-other-death')
        }


        gunshotDeath.innerHTML = gunshotCause + '%'
        taserDeath.innerHTML = taserCause + '%'
        otherDeath.innerHTML = otherCause + '%'
    }
}


class CitiesDatas{
    constructor(cities, when){

        this.cities = cities
        this.when = when || ''
        this.countsCity = {}

        this.init()
    }
    init()
    {
        this.cities.forEach(x => {
            this.countsCity[x] = (this.countsCity[x] || 0)+1
        })

        const newArray = Object.entries(this.countsCity)

        newArray.sort((a,b) => { return a[1]-b[1] })

        let murderCityCount = 0
        let murderCityName = []

        newArray.forEach( value => {
            if(value[1] > murderCityCount){
                murderCityCount = value[1]
                murderCityName.push([value[0], value[1]])
            }
        })

        murderCityName.sort((a,b) => { return b[1]-a[1] })

        let selcteurValue = 5

        murderCityName.forEach(element => {
            if( selcteurValue > 0) {
                d3.select(`.svg-${selcteurValue}${this.when} text`)
                .text(element[0])

                d3.select(`.svg-${selcteurValue}${this.when} rect`)
                .on(('mouseover'),function(d){
                    d3.select(this.parentNode).select('.this-text').text(`${element[1]} killed`)
                 })
                .on(('mouseout'),function(d){
                    d3.select(this.parentNode).select('.this-text').text(element[0])
                 })

                selcteurValue--
            }else return
        })
    }
}


class GenderDeath{
    constructor(malePercentage, femalePercentage, when){
        this.malePercentage = malePercentage
        this.femalePercentage = femalePercentage
        this.when = when || ''


        if (this.when === '-state') {
            const percentGender = d3.select('.percent-gender-state')

            percentGender.selectAll("*").remove();

            percentGender.append("stop")
            .attr("offset", this.malePercentage + "%")
            .attr("stop-opacity", 1)
            .attr("stop-color", "#3021D9")
            percentGender.append("stop")
            .attr("offset",  this.femalePercentage + "%")
            .attr("stop-opacity", 1)
            .attr("stop-color", "#FF005C")
        }
        else{
            const percentGender = d3.select('.percent-gender')

            console.log(percentGender, this.malePercentage,  this.femalePercentage);

            percentGender.append("stop")
            .attr("offset", this.malePercentage + "%")
            .attr("stop-opacity", 1)
            .attr("stop-color", "#3021D9")
            percentGender.append("stop")
            .attr("offset",  this.femalePercentage + "%")
            .attr("stop-opacity", 1)
            .attr("stop-color", "#FF005C")
        }
    }
}


class VictimDatas{
    constructor(victimDatas, when){

        this.victimDatas = victimDatas

        this.when = when || ''

        this.victim =  this.victimDatas[Math.floor(Math.random() * this.victimDatas.length)]

        console.log('La victime =', this.victim);

        if (this.victim === undefined) this.nodata()
        else this.init()
    }
    init()
    {

        let containerVictim

        if (this.when === '-state') containerVictim =document.querySelector('.container-victim-information-state')
        else containerVictim = document.querySelector('.container-victim-information')

        const imageVictim = containerVictim.querySelector('.container-victim-image')

        image.src = this.victim.image
        imageVictim.appendChild(image)


        const nameVictim = containerVictim.querySelector('.container-victim-depth h2')
        const depthVictim = containerVictim.querySelectorAll('.container-victim-depth h4')

        const incidentVictim = containerVictim.querySelectorAll('.container-victim-incident h4 span')
        const causeVictim = containerVictim.querySelectorAll('.container-victim-cause h4 span')

        nameVictim.textContent = this.victim.name

        depthVictim.forEach( depth => {
            if( depthVictim[0] === depth ) depth.textContent =  `${this.victim.age} years old`
            else depth.textContent = this.victim.race
        })

        incidentVictim.forEach(incident => {
            if( incidentVictim[0] === incident ) incident.textContent = this.victim.date
            else incident.textContent = this.victim.city
        })
        causeVictim.forEach(cause => {
            if( causeVictim[0] === cause ) cause.textContent = this.victim.cause
            else cause.textContent = this.victim.description
        })
    }
    nodata(){

    }
}

const containerMap = document.querySelector('.container-map')
const goToStats = document.querySelectorAll('.go-to-stats')
// clickOut

const changeLink = () => {
    console.log('ininininininiininin');

    goToStats.forEach(link => {
        if(link.classList.contains('noneDisplay')){
            link.classList.remove('noneDisplay')
        }else link.classList.add('noneDisplay')
    })

}



const goToStatsUs = document.querySelector('.go-to-stats')
const dataForUS = document.querySelector('.data-for-us')

const goToStatsState = document.querySelector('.go-to-stats-state')
const dataForState = document.querySelector('.data-for-state')

const goToMap = document.querySelectorAll('.container-back-to-the-map a')


let nextSlide = 0

goToStatsState.addEventListener('click', () => {

    if (dataState.totalData != 0) dataState.stateAllData()
    containerMap.classList.remove('flexDisplay')
    dataForState.classList.add('flexDisplay')
})

goToStatsUs.addEventListener('click', () => {

    containerMap.classList.remove('flexDisplay')
    dataForUS.classList.add('flexDisplay')
})

goToMap.forEach( link => {
    link.addEventListener('click', () => {
        if (dataForUS.classList.contains('flexDisplay'))dataForUS.classList.remove('flexDisplay')
        if (goToStatsUs.classList.contains('flexDisplay'))goToStatsUs.classList.remove('flexDisplay')
        containerMap.classList.add('flexDisplay')
    })
})



const arrowButton = document.querySelectorAll('.container-arrow')

const dataExplorer = document.querySelectorAll('.data-explorer')
const dataExplorerState = document.querySelectorAll('.data-explorer-state')

const dataTitle = document.querySelector('.title-data')

arrowButton.forEach(arrow =>
    {
        arrow.addEventListener('click', () => {
            if (dataForUS.classList.contains('flexDisplay')) arrowFunction(dataExplorer, arrow)
            else if (dataForState.classList.contains('flexDisplay'))  arrowFunction(dataExplorerState, arrow)
        })
    })

const arrowFunction = (explorer, arrow) => {
    explorer.forEach( explore => {
        if(!explore.classList.contains('isNotIn')){
            explore.classList.add('isNotIn')
            return
        }
    })
    if (arrow === arrowButton[0]) {
        nextSlide--
        if (nextSlide < 0)nextSlide = 4
        explorer[nextSlide].classList.remove('isNotIn')
        // dataTitle.textContent = titls[nextSlide]

    }else{
        nextSlide++
        if (nextSlide === 5) nextSlide = 0
        explorer[nextSlide].classList.remove('isNotIn')
        // dataTitle.textContent = titls[nextSlide]
    }
}