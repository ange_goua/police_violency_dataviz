// @author brunoimbrizi / http://brunoimbrizi.com

precision highp float;

uniform sampler2D uTexture;

varying vec2 vPUv;
varying vec2 vUv;

void main() {
	vec4 color = vec4(0.0);
	vec2 uv = vUv;
	vec2 puv = vPUv;

	// pixel color
	vec4 colA = texture2D(uTexture, puv);

	// greyscale
	float grey = colA.r * 0.21 + colA.g * 0.71 + colA.b * 0.07;
	vec4 colB = vec4(grey, grey, grey, 1.0);

	// circle
	float border = 0.3;
    // float radius = .25;
	// float dist = radius ;
	float radius = 0.5;
    // distance(uv,vec2(0.4)) + distance(uv,vec2(0.6));

    vec2 pos = vec2(0.5)-uv;
    float r = length(pos)*2.0;
    float a = atan(pos.y,pos.x);
	float dist = a + r*1.25 ;

    // vec2 pos = vec2(0.5)-uv;
    // float r = length(pos)*2.0;
    // float a = atan(pos.y,pos.x);
    // float dist = smoothstep(-.5,1., cos(a*10.))*0.2+0.5-r*.5;
    //  smoothstep(-.5,1., cos(a*10.))*0.2+0.5;

    //float dist = radius - distance(uv, vec2(0.5));
	float t = smoothstep(0.0, border, dist);

	// final color
	color = colB;
	color.a = t;

	gl_FragColor = color;
}