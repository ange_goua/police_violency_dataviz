import * as THREE from 'three';
import { TweenLite } from 'gsap/all';


import InteractiveControls from './InteractiveControls';
import Particles from './particle/Particle';
import imageSlide1 from '../../images/image-slide-1.png';
import imageSlide2 from '../../images/image-slide-2.png';
import imageSlide3 from '../../images/image-slide-3.png';
import imageSlide4 from '../../images/image-slide-4.png';
import imageSlide5 from '../../images/image-slide-5.png';
import imageSlide6 from '../../images/image-slide-6.png';


export default class WebGLView {

	constructor(app) {
		this.app = app;

		this.imageSlides = [
            imageSlide1,
            imageSlide2,
            imageSlide3,
            imageSlide4,
            imageSlide5,
            imageSlide6
        ];

        this.sizes = {}
        this.cursor = {}

		this.initThree();
		this.initParticles();
		this.initControls();

		this.goto(0);
	}

	initThree() {

        /**
         * Sizes
         */
        this.sizes.width = window.innerWidth
        this.sizes.height = window.innerHeight


        /**
         * Scene
         */

        this.scene = new THREE.Scene()

		// camera
		this.camera = new THREE.PerspectiveCamera(50, this.sizes.width / this.sizes.height, 1, 1000);
		this.camera.position.z = 300;

		// renderer
        this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });

        // clock
		this.clock = new THREE.Clock(true);
	}

	initControls() {
		this.interactive = new InteractiveControls(this.camera, this.renderer.domElement);
	}

	initParticles() {
		this.particles = new Particles(this);
		this.scene.add(this.particles.container);
	}

	// ---------------------------------------------------------------------------------------------
	// PUBLIC
	// ---------------------------------------------------------------------------------------------

	update() {
		const delta = this.clock.getDelta();

		if (this.particles) this.particles.update(delta);
	}

	draw() {
		this.renderer.render(this.scene, this.camera);
	}


	goto(index) {
        // init next

		if (this.currSample == null) this.particles.init(this.imageSlides[index]);
		// hide curr then init next
		else {
			this.particles.hide(true).then(() => {
				this.particles.init(this.imageSlides[index]);
            });
		}

		this.currSample = index;
	}

	next() {
		if (this.currSample < this.imageSlides.length - 1) this.goto(this.currSample + 1);
		else this.goto(0);
	}

	// ---------------------------------------------------------------------------------------------
	// EVENT HANDLERS
	// ---------------------------------------------------------------------------------------------

	resize() {
        if (!this.renderer) return;

        this.sizes.width = window.innerWidth
        this.sizes.height = window.innerHeight

		this.camera.aspect = window.innerWidth / window.innerHeight;
		this.camera.updateProjectionMatrix();

		this.fovHeight = 2 * Math.tan((this.camera.fov * Math.PI) / 180 / 2) * this.camera.position.z;

		this.renderer.setSize(this.sizes.width , this.sizes.height);

		if (this.interactive) this.interactive.resize();
		if (this.particles) this.particles.resize();
	}
}
