import './styles/index.styl'

export default class Share{
    constructor(){

        this.shareMedia = document.querySelector('.share-logo')
        this.shareList = document.querySelector('.share-list')
        this.shareMediaActiv = document.querySelector('.unactiv')

        this.shareMedia.addEventListener("click", () => this.onClick() )

    }
    onClick()
    {
        if(this.shareMediaActiv.classList.contains('unactiv'))
        {
            this.shareList.style.transform = "translateY(-50%)"
            window.setTimeout(()=> this.shareMediaActiv.classList.remove('unactiv') ,300 )
        }
        else
        {
            this.shareList.style.transform = "translateY(0%)"
            window.setTimeout(()=> this.shareMediaActiv.classList.add('unactiv') ,100 )
        }
    }
}

const share = new Share()
