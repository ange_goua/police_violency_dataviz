import "./styles/statistics.styl"
import "./styles/index.styl"
import Share from './index.js'
import DataConverterUs from "./scripts/usMap.js"


let isCAlled = false, dataConverterUs

const share = new Share()

const containerYears = document.querySelector('.container-years')
containerYears.classList.add('flexDisplay')

const containerMap = document.querySelector('.container-map')


const yearsText = containerYears.querySelector('.year-js')
const yearEnter = containerYears.querySelector('.container-enter')

const choseStat = document.querySelector('.chose-stats')
const buttonSelect = document.querySelectorAll('.chose-stats h4')

const textYear = document.querySelectorAll('.container-text-year')

const regex = new RegExp('^[3-9]');

let yearChoosen = '201'

document.addEventListener('click', (e) => {
        if (!yearsText.contains(e.target)) yearsText.classList.remove('cliked-number')
        else yearsText.classList.add('cliked-number')
        if( yearEnter.contains(e.target)) if(!isCAlled)goTomap()
})

window.addEventListener('keydown', (e) => {
    if (regex.test(e.key)) {
        yearsText.classList.add('cliked-number')
        yearsText.textContent = e.key
    }
    if (e.key === 'Escape') goToyear()

    if(e.key === 'Enter') if(!isCAlled)goTomap()
})

const goTomap = () => {

    isCAlled = true

    dataConverterUs = new DataConverterUs()

    yearsText.classList.remove('cliked-number')
    yearChoosen += yearsText.textContent

    yearEnter.classList.add('cliked-number')

    setTimeout(() => {
        containerYears.classList.remove('flexDisplay')

        yearEnter.classList.remove('cliked-number')

    },1000)

    containerMap.classList.add('flexDisplay')
    dataConverterUs.getDataState(yearChoosen)
    displayYear(yearChoosen)
}

const goToyear = () => {

    isCAlled = false
    yearChoosen = '201'

    dataConverterUs.firstDelete()
    containerYears.classList.add('flexDisplay')
}

const displayYear = (yearChoosen) => {
    textYear.forEach(text =>
        text.textContent = yearChoosen
    )
}